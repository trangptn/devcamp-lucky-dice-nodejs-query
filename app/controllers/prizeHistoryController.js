const mongoose = require('mongoose');

const prizeHistoryModel = require('../models/prizeHistoryModel');

const createPrizeHistory = async (req, res) => {
    const {
        reqUser,
        reqPrize
    } = req.body

    if (!mongoose.Types.ObjectId.isValid(reqUser)) {
        res.status(400).json({
            message: "Ma user khong hop le"
        })
        return false
    }
    if (!mongoose.Types.ObjectId.isValid(reqPrize)) {
        res.status(400).json({
            message: "Ma prize khong hop le"
        })
        return false;
    }

    try {
        var newPrizeHistory = {
            user: reqUser,
            prize: reqPrize
        }
        const result = await prizeHistoryModel.create(newPrizeHistory);

        res.status(201).json({
            message: "Tao thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllPrizeHistory = async (req, res) => {
    const user=req.query.user;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(user))
    {
        condition.user=user;
    }
    try {
        const result = await prizeHistoryModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
const getPrizeHistoryById = async (req, res) => {
    const prizehistoryid = req.params.prizehistoryid;

    if (!mongoose.Types.ObjectId.isValid(prizehistoryid)) {
        res.status(400).json({
            message: " PrizeHistory Id khong hop le"
        })
    }
    const result = await prizeHistoryModel.findById(prizehistoryid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updatePrizeHistoryById = async (req, res) => {
    const prizehistoryid = req.params.prizehistoryid;
    if (!mongoose.Types.ObjectId.isValid(prizehistoryid)) {
        res.status(400).json({
            message: "Id khong hop le"
        })
    }
    const { reqUser, reqPrize } = req.body;
    const reqUpdateAt = Date.now();

    if (!mongoose.Types.ObjectId.isValid(reqUser)) {
        res.status(400).json({
            message: "Id  user khong hop le"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(reqPrize)) {
        res.status(400).json({
            message: "Id prize khong hop le"
        })
    }
    try {
        var newPrizeHistory = {};
        if(reqUser)
        {
            newPrizeHistory.user=reqUser;
        }
        if(reqPrize)
        {
            newPrizeHistory.prize=reqPrize;
        }
        newPrizeHistory.updatedAt = reqUpdateAt;
        
        const result = await prizeHistoryModel.findByIdAndUpdate(prizehistoryid,newPrizeHistory);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin prizeHistory"
            })
        }
    }
    catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deletePrizeHistoryById = async (req,res) =>{
    const prizehistoryid= req.params.prizehistoryid;

    try{
        const result = await prizeHistoryModel.findByIdAndDelete(prizehistoryid);
        if(result)
        {
            res.status(200).json({
                message:"Xoa thong tin thanh cong"
            })
        }
        else{
            res.status(400).json({
                message:"Khong tim thay thong tin prizeHistory"
            })
        }
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}