const express= require('express');

const router= express.Router();

const{
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}=require('../controllers/voucherHistoryController');


router.post("/",createVoucherHistory);
router.get("/",getAllVoucherHistory);
router.get("/:voucherhistoryid",getVoucherHistoryById);
router.put("/:voucherhistoryid",updateVoucherHistoryById);
router.delete("/:voucherhistoryid",deleteVoucherHistoryById);
module.exports=router;