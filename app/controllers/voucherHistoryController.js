const mongoose = require('mongoose');

const voucherHistoryModel = require('../models/voucherHisoryModel');

const createVoucherHistory = async (req, res) => {
    const {
        reqUser,
        reqVoucher
    } = req.body

    if (!mongoose.Types.ObjectId.isValid(reqUser)) {
        res.status(400).json({
            message: "Ma user khong hop le"
        })
        return false
    }
    if (!mongoose.Types.ObjectId.isValid(reqVoucher)) {
        res.status(400).json({
            message: "Ma voucher khong hop le"
        })
        return false;
    }

    try {
        var newVoucherHistory = {
            user: reqUser,
            voucher: reqVoucher
        }
        const result = await voucherHistoryModel.create(newVoucherHistory);

        res.status(201).json({
            message: "Tao thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllVoucherHistory = async (req, res) => {
    const user=req.query.user;
    const condition={};
    if(mongoose.Types.ObjectId.isValid(user))
    {
        condition.user=user;
    }
    try {
        const result = await voucherHistoryModel.find(condition);
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
const getVoucherHistoryById = async (req, res) => {
    const voucherhistoryid = req.params.voucherhistoryid;

    if (!mongoose.Types.ObjectId.isValid(voucherhistoryid)) {
        res.status(400).json({
            message: " VoucherHistory Id khong hop le"
        })
    }
    const result = await voucherHistoryModel.findById(voucherhistoryid);
    try {
        res.status(200).json({
            message: "Lay du lieu thanh cong",
            data: result
        })
    }
    catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateVoucherHistoryById = async (req, res) => {
    const voucherhistoryid = req.params.voucherhistoryid;
    if (!mongoose.Types.ObjectId.isValid(voucherhistoryid)) {
        res.status(400).json({
            message: "Id khong hop le"
        })
    }
    const { reqUser, reqVoucher } = req.body;
    const reqUpdateAt = Date.now();

    if (!mongoose.Types.ObjectId.isValid(reqUser)) {
        res.status(400).json({
            message: "Id  user khong hop le"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(reqVoucher)) {
        res.status(400).json({
            message: "Id voucher khong hop le"
        })
    }
    try {
        var newVoucherHistory = {};
        if(reqUser)
        {
            newVoucherHistory.user=reqUser;
        }
        if(reqVoucher)
        {
            newVoucherHistory.voucher=reqVoucher;
        }
        newVoucherHistory.updatedAt = reqUpdateAt;
        
        const result = await voucherHistoryModel.findByIdAndUpdate(voucherhistoryid,newVoucherHistory);
        if (result) {
            res.status(200).json({
                message: "Cap nhat thong tin thanh cong",
                data: result
            })
        }
        else {
            res.status(400).json({
                message: "Khong tim thay thong tin voucherHistory"
            })
        }
    }
    catch (err) {
        console.log(err);
        res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const deleteVoucherHistoryById = async (req,res) =>{
    const voucherhistoryid= req.params.voucherhistoryid;

    try{
        const result = await voucherHistoryModel.findByIdAndDelete(voucherhistoryid);
        if(result)
        {
            res.status(200).json({
                message:"Xoa thong tin thanh cong"
            })
        }
        else{
            res.status(400).json({
                message:"Khong tim thay thong tin voucherHistory"
            })
        }
    }
    catch(err)
    {
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}