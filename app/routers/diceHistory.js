const express= require('express');

const router= express.Router();

const{
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}=require('../controllers/diceHistoryController');


router.post("/",createDiceHistory);
router.get("/",getAllDiceHistory);
router.get("/:dicehistoryid",getDiceHistoryById);
router.put("/:dicehistoryid",updateDiceHistoryById);
router.delete("/:dicehistoryid",deleteDiceHistoryById);
module.exports=router;