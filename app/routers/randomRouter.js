const express= require('express');

const router= express.Router();

const path=require('path');

const {getDateTime,getMethod}=require("../middlewares/dateTimeMiddleware");

const {getRandom}=require("../controllers/randomController")

router.get("/random-number",getDateTime,getMethod,getRandom);

router.get("/",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname+ '/../views/lucky-dice.html'));
})

router.use(express.static(__dirname+'/../views'));
module.exports=router;