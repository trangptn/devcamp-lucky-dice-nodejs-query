const express= require('express');

const router=express.Router();

const {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}=require("../controllers/prizeController");

router.post("/",createPrize);
router.get("/",getAllPrize);
router.get("/:prizeid",getPrizeById);
router.put("/:prizeid",updatePrizeById);
router.delete("/:prizeid",deletePrizeById);
module.exports=router;