const express= require('express');

const router= express.Router();

const{
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}=require('../controllers/prizeHistoryController');


router.post("/",createPrizeHistory);
router.get("/",getAllPrizeHistory);
router.get("/:prizehistoryid",getPrizeHistoryById);
router.put("/:prizehistoryid",updatePrizeHistoryById);
router.delete("/:prizehistoryid",deletePrizeHistoryById);
module.exports=router;